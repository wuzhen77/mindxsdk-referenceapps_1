# FCOS

## 1 介绍

本开发项目演示FCOS模型实现目标检测。本系统基于mxVision SDK进行开发，以昇腾Atlas300卡为主要的硬件平台，主要应用于在CPU上实现实时目标检测。待检测的图片中物体不能被遮挡太严重，并且物体要完全出现在图片中。图片亮度不能过低。输入一张图片，最后会输出图片中能检测到的物体。项目主要流程未：

1.环境搭建；
2.模型转换；
3.生成后处理插件；
4.进行精度、性能对比。

### 1.1支持的产品

本产品以昇腾310（推理）卡为硬件平台。

### 1.2支持的版本

该项目支持的SDK版本为2.0.4，CANN版本为5.0.4。

### 1.3软件方案介绍

基于MindXSDK的FCOS目标检测的业务流程为：

1. 将待检测的图片通过ffmpeg将其转换成为1344*800格式为jpg的图片；
2. 然后通过图片解码插件"mxpi_imagedecoder0"进行视频解码；
3. 然后通过"mxpi_imageresize0"插件将图像大小改变为满足模型检测哟啊求的输入图片的大小要求；
4. 放缩后的图片输入模型推理插件mxpi_tensorinfer进行处理；
5. 将经过模型推理输出时候的张量数据流输入到mxpi_objectpostprocessor中，对FCOS目标检测模型推理输出的张量进行后处理；
6. 处理完毕之后将数据传入mxpi_dataserialize0插件中，将stream结果组装成json字符串输出。

下表为系统方案各个子系统功能的描述：

| 序号 | 子系统     | 功能描述                                                                                                               |
| ---- | ---------- | ---------------------------------------------------------------------------------------------------------------------- |
| 1    | 图片输入   | 传入图片，修改图片的大小和格式为符合模型要求的格式                                                                     |
| 2    | 图片解码   | 用于对图片进行解码                                                                                                     |
| 3    | 图像放缩   | 通过mxpi_imageresize插件对图片进行放缩，达到模型传入图片的格式要求                                                     |
| 4    | 模型推理   | 将已经处理好的图片传入到mxpi_tensorinfer中，使用目标检测模型FCOS进行推理，得到推理的张量数据流                         |
| 5    | 模型后处理 | 将模型推理得到的张量数据流传入mxpi_objectpostprocessor中进行张量后处理。对模型输出的目标框进行去重，排序和筛选等工作。 |
| 6    | 组装字符串 | stream结果组装成json字符串输出。                                                                                       |

### 1.4代码目录结构与说明

本项目名为FCOS目标检测，项目的目录如下所示：

```
|- model
|	|- aipp_FCOS.aippconfig	//模型转换配置文件
|	|- fcos.onnx				//onnx模型
|	|_ Fcos_tf_bs.cfg
|- pipeline
|	|_ FCOSdetection.pipeline
|- plugin
|	|_FCOSPostprocess
|		|- CMakeLists.txt
|		|- FCOSDetectionPostProcess.cpp
|		|- FCOSDetectionPostProcess.h
|		|_ build.sh
|- build.sh
|- main.py
|_ run.sh
```

### 1.5技术实现流程图

本项目实现对输入的图片进行目标检测，整体流程如下：

![未命名文件_29_.png](https://s2.loli.net/2022/07/15/ysPG4jKONf7ZEWI.png)

## 2环境依赖

推荐系统为ubuntu 18.04,环境软件和版本如下：

| 软件名称            | 版本  | 说明                          | 获取方式                                                          |
| ------------------- | ----- | ----------------------------- | ----------------------------------------------------------------- |
| MindX SDK           | 2.0.4 | mxVision软件包                | [点击打开链接](https://www.hiascend.com/software/Mindx-sdk)       |
| ubuntu              | 18.04 | 操作系统                      | 请上ubuntu官网获取                                                |
| Ascend-CANN-toolkit | 5.0.4 | Ascend-cann-toolkit开发套件包 | [点击打开链接](https://www.hiascend.com/software/cann/commercial) |

在项目开始运行前需要设置环境变量：

```
. /usr/local/Ascend/ascend-toolkit/set_env.sh
. ${SDK安装路径}/mxVision/set_env.sh
```

## 3 软件依赖

项目运行过程中涉及到的第三方软件依赖如下表所示：

| 软件名称 | 版本       | 说明                                 | 使用教程                                                                                                                                                                                                        |
| -------- | ---------- | ------------------------------------ | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| ffmpeg   | 2021-07-21 | 实现图片的格式转换，改变图片的长宽等 | [点击打开链接](https://gitee.com/ascend/mindxsdk-referenceapps/blob/master/docs/%E5%8F%82%E8%80%83%E8%B5%84%E6%96%99/pc%E7%AB%AFffmpeg%E5%AE%89%E8%A3%85%E6%95%99%E7%A8%8B.md#https://ffmpeg.org/download.html) |

## 4 模型转换

本项目使用的模型是FCOS目标检测模型这个模型是一个无anchor检测器。FCOS直接把预测特征图上的每个位置$(x,y)$当作训练样本，若这个位置在某个ground truth box的内部，则视为正样本，该位置的类别标签$c*$对应这个box的类别，反之则视为负样本。这个网络的输出为目标框的左上角坐标、右下角坐标、类别和置信度。本项目的onnx模型可以直接[下载](https://www.hiascend.com/zh/software/modelzoo/models/detail/1/6fcc4747a48245d29351c26cd052dd13)。下载后，里面自带的om模型是不可以直接使用的，需要通过模型转换工具ATC将onnx模型转换成为om模型，模型转换工具的使用说明参考[链接](https://support.huaweicloud.com/tg-cannApplicationDev330/atlasatc_16_0005.html)。

模型转换步骤如下：

1.从下载链接处下载onnx模型至FCOS/models文件夹下。

2.进入models文件夹目录下，设置环境变量如下：

```
export install_path=/usr/local/Ascend/ascend-toolkit/latest
export PATH=${install_path}/atc/ccec_compiler/bin:${install_path}/atc/bin:$PATH
export PYTHONPATH=${install_path}/atc/python/site-packages:${install_path}/atc/python/site-packages/auto_tune.egg/auto_tune:${install_path}/atc/python/site-packages/schedule_search.egg
export LD_LIBRARY_PATH=${install_path}/atc/lib64:$LD_LIBRARY_PATH
export ASCEND_OPP_PATH=${install_path}/opp
```

设置完环境变量之后，就进行模型的转换：

模型转换语句如下：

```
atc --model=fcos.onnx --framework=5 --soc_version=Ascend310 --input_format=NCHW --input_shape="input:1,3,800,1344" --output=fcos_om --insert_op_conf=aipp_FCOS.aippconfig --precision_mode=allow_fp32_to_fp16
```

执行完该命令之后，会在models文件夹下生成.om模型，并且转换成功之后会在终端输出：

```
ATC start working now, please wait for a moment.
ATC run success, welcome to the next use.
```

模型转换的aipp配置文件的配置如下：

```
aipp_op {
       aipp_mode : static
       src_image_size_w : 1344
       src_image_size_h : 800
       rbuv_swap_switch :false
       input_format : YUV420SP_U8
       csc_switch : true
       rbuv_swap_switch : false
       crop :true
       load_start_pos_w :0
       load_start_pos_h :0
       crop_size_w :1333
       crop_size_h :800
}
```

## 5准备

### 步骤1

按照第三节的软件依赖安装ffmpeg，按照使用说明将输入的图片转换成为1344*800的jpg格式图片，并上传到FCOS文件夹下。然后修改main.py文件里面的图片路径为待检测的图片路径。

### 步骤2

进入FCOS/plugin目录，在该目录下运行下列命令：

```
mkdir build
cd build
cmake ..
make -j
make install
```

执行完毕之后就生成了后处理插件。这个后处理插件可以直接在pipeline中使用。

## 6编译与运行

### 步骤1

按照第二小节设置好运行前的环境变量。

### 步骤2

按照第四小节模型转换中的步骤获取om模型文件，防止在FCOS/models目录下。

### 步骤3

修改main.py文件中的图片路径。

### 步骤4 编译

进入FCOS目录，在该目录下执行命令：

```
bash build.sh
```

执行命令成功之后会在FCOS/plugins/FCOSpostprocess目录下生成build文件夹，并且在${MindX_SDK安装路径}/mxVision/lib/modelpostprocessors下会生成对应的后处理.so文件。

### 步骤5 运行：

在FCOS目录下执行命令：

```
python3 main.py
```

最后生成的结果会在FCOS文件夹目录下result.jpg图片中。

## 7常见问题：
### 7.1 模型路径配置问题：
#### 问题描述：
检测过程中用到的模型以及模型后处理插件需配置路径属性。

#### 后处理插件以及模型推理插件配置例子：

```json
// 模型推理插件
            "mxpi_tensorinfer0": {
                "props": {
                    "dataSource": "mxpi_imageresize0",
                    "modelPath": "${模型路径}/fcos_bs1.om"
                },
                "factory": "mxpi_tensorinfer",
                "next": "mxpi_objectpostprocessor0"
            },
// 模型后处理插件
            "mxpi_objectpostprocessor0":{
                "props": {
                    "dataSource" : "mxpi_tensorinfer0",
                    "postProcessConfigPath": "./models/Fcos_tf_bs.cfg",
                    "postProcessLibPath": "${MX_SDK_HOME}/lib/modelpostprocessors/libFCOSDetectionPostProcess.so"
                },
                "factory": "mxpi_objectpostprocessor",
                "next": "mxpi_dataserialize0"
            },
```

### 7.2 在main.py中图片路径：

在传入待检测图片时候需要注意其在main.py中的路径。
```python
    if os.path.exists('${图片路径}/image.jpg') != 1:
        print("The test image does not exist.")

    with open("${图片路径}/image.jpg", 'rb') as f:
        dataInput.data = f.read()
```

### 7.3 在传入图片时候需要修改图片的大小以及格式：
修改图片大小为1344*800，图片格式为jpg格式。在得到图片时候可以采用ffmpeg将图片转换成为需要的格式，语句如下：
(假设图片为webp格式。)
```
ffmpeg -i image.webp -s 1344*800 image.jpg
```
之后将转换成功之后的图片进行推理即可。