#!/usr/bin/env python
# coding=utf-8
# Copyright(C) 2022. Huawei Technologies Co.,Ltd. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import json
import os
import cv2
import numpy as np

import MxpiDataType_pb2 as MxpiDataType
from StreamManagerApi import StreamManagerApi, MxDataInput, StringVector

if __name__ == '__main__':
    streamManagerApi = StreamManagerApi()
    # create a StreamManager and init it
    ret = streamManagerApi.InitManager()
    if ret != 0:
        print("Failed to init Stream manager, ret=%s" % str(ret))
        exit()

    # create pipeline
    with open("/home/dongyu3/wuzhFCOS/pipeline/test.pipeline", 'rb') as f:
        pipelineStr = f.read()
    ret = streamManagerApi.CreateMultipleStreams(pipelineStr)
    if ret != 0:
        print("Failed to create Stream, ret=%s" % str(ret))
        exit()

    # create detection object
    dataInput = MxDataInput()
    if os.path.exists('${图片路径}/image.jpg') != 1:
        print("The test image does not exist.")

    with open("${图片路径}/image.jpg", 'rb') as f:
        dataInput.data = f.read()

    STREAMNAME = b'detection'
    INPLUGINID = 0
    # put the detection object to stream
    uniqueId = streamManagerApi.SendData(STREAMNAME, INPLUGINID, dataInput)

    if uniqueId < 0:
        print("Failed to send data to stream.")
        exit()

    keys = [b"mxpi_objectpostprocessor0", b"mxpi_imagedecoder0"]
    keyVec = StringVector()
    for key in keys:
        keyVec.push_back(key)

    # get the output data from the stream plugin
    infer_result = streamManagerApi.GetProtobuf(STREAMNAME, 0, keyVec)

    if infer_result.size() == 0:
        print("infer_result is null")
        exit()

    if infer_result[0].errorCode != 0:
        print("GetProtobuf error. errorCode=%d" % (infer_result[0].errorCode))
        exit()

    YUV_BYTES_NU = 3
    YUV_BYTES_DE = 2
    # get output information from mxpi_objectpostprocessor0
    objectList = MxpiDataType.MxpiObjectList()
    objectList.ParseFromString(infer_result[0].messageBuf)
    print(objectList)

    # get the output information from mxpi_imagedecoder0
    visionList = MxpiDataType.MxpiVisionList()
    visionList.ParseFromString(infer_result[1].messageBuf)

    vision_data = visionList.visionVec[0].visionData.dataStr
    visionInfo = visionList.visionVec[0].visionInfo

    img_yuv = np.frombuffer(vision_data, np.uint8)

    img_bgr = img_yuv.reshape(
        visionInfo.heightAligned * YUV_BYTES_NU // YUV_BYTES_DE,
        visionInfo.widthAligned)
    img = cv2.cvtColor(img_bgr, getattr(cv2, "COLOR_YUV2BGR_NV12"))

    # print reasoning result
    result = objectList.objectVec
    for x in result:
        TEXTNAME = "{}{}".format(str(round(x.classVec[0].confidence, 4)), " ")
        TEXTNAME += x.classVec[0].className
        cv2.putText(img, TEXTNAME, (int(x.x0) + 10, int(x.y0) + 10),
                    cv2.FONT_HERSHEY_SIMPLEX, 1.0, (255, 0, 0), 1)
        cv2.rectangle(img, (int(x.x0), int(x.y0)), (int(x.x1), int(x.y1)),
                      (255, 0, 0), 2)
    imgDst = cv2.resize(img, (1344, 800), interpolation=cv2.INTER_AREA)
    cv2.imwrite("./result.jpg", imgDst)
    # destroy streams
    streamManagerApi.DestroyAllStreams()
