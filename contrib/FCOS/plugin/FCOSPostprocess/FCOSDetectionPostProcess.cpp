/*
 * Copyright(C) 2022. Huawei Technologies Co.,Ltd. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "FCOSDetectionPostProcess.h"

#include "MxBase/Log/Log.h"

namespace {
const float RATE = 0.3;
const uint32_t L = 0;
const uint32_t T = 1;
const uint32_t R = 2;
const uint32_t B = 3;
const uint32_t CENTERPOINT = 4;
const std::string CATEGORIES[80] = {  // all classes that can be detected.
    "person",        "bicycle",      "car",
    "motorcycle",    "airplane",     "bus",
    "train",         "truck",        "boat",
    "traffic light", "fire hydrant", "stop sign",
    "parking meter", "bench",        "bird",
    "cat",           "dog",          "horse",
    "sheep",         "cow",          "elephant",
    "bear",          "zebra",        "giraffe",
    "backpack",      "umbrella",     "handbag",
    "tie",           "suitcase",     "frisbee",
    "skis",          "snowboard",    "sports ball",
    "kite",          "baseball bat", "baseball glove",
    "skateboard",    "surfboard",    "tennis racket",
    "bottle",        "wine glass",   "cup",
    "fork",          "knife",        "spoon",
    "bowl",          "banana",       "apple",
    "sandwich",      "orange",       "broccoli",
    "carrot",        "hot dog",      "pizza",
    "donut",         "cake",         "chair",
    "couch",         "potted plant", "bed",
    "dining table",  "toilet",       "tv",
    "laptop",        "mouse",        "remote",
    "keyboard",      "cell phone",   "microwave",
    "oven",          "toaster",      "sink",
    "refrigerator",  "book",         "clock",
    "vase",          "scissors",     "teddy bear",
    "hair drier",    "toothbrush"};
const float BESTMINCONF[80] = {
    // the best min-confidence of each class.
    0.47482219338417053, 0.46463844180107117, 0.4777076840400696,
    0.4707475006580353,  0.5125738978385925,  0.5007652044296265,
    0.5468990802764893,  0.46303099393844604, 0.4539012610912323,
    0.43332818150520325, 0.5023174285888672,  0.5248752236366272,
    0.5437473654747009,  0.44714388251304626, 0.4194680452346802,
    0.5178298354148865,  0.5087228417396545,  0.5021755695343018,
    0.49227115511894226, 0.4751800000667572,  0.5202335119247437,
    0.5012319087982178,  0.5089765191078186,  0.5422378778457642,
    0.45138806104660034, 0.49138879776000977, 0.4207921326160431,
    0.4712490439414978,  0.4415128827095032,  0.4791252017021179,
    0.4222373962402344,  0.4508270025253296,  0.4580649137496948,
    0.4603237509727478,  0.4614591598510742,  0.5235602855682373,
    0.48501554131507874, 0.5058678984642029,  0.4977407157421112,
    0.45824387669563293, 0.45555758476257324, 0.4774158298969269,
    0.4558144807815552,  0.399429589509964,   0.434289813041687,
    0.4812376797199249,  0.43979522585868835, 0.4241463541984558,
    0.4858604073524475,  0.44542714953422546, 0.4538525640964508,
    0.452717125415802,   0.471712201833725,   0.49695995450019836,
    0.44091302156448364, 0.46602892875671387, 0.453443706035614,
    0.464419960975647,   0.449857234954834,   0.45533207058906555,
    0.4378446936607361,  0.5584923028945923,  0.5448692440986633,
    0.4828561544418335,  0.5418091416358948,  0.46843957901000977,
    0.4722832441329956,  0.46560579538345337, 0.5242034196853638,
    0.4659145474433899,  0.4246722459793091,  0.48213040828704834,
    0.48866209387779236, 0.4044249355792999,  0.5392034649848938,
    0.48504579067230225, 0.43217378854751587, 0.4814004898071289,
    0.29594743251800537, 0.4143834114074707,
};
}  // namespace
namespace MxBase {
FCOSPostProcess &FCOSPostProcess::operator=(const FCOSPostProcess &other) {
  if (this == &other) {
    return *this;
  }
  ObjectPostProcessBase::operator=(other);
  return *this;
}

APP_ERROR FCOSPostProcess::Init(
    const std::map<std::string, std::shared_ptr<void>> &postConfig) {
  LogInfo << "Start to Init FCOSDetectionPostProcess";
  APP_ERROR ret = ObjectPostProcessBase::Init(postConfig);
  if (ret != APP_ERR_OK) {
    LogError << GetError(ret)
             << "Fail to superInit in FCOSDetectionPostProcess.";
    return ret;
  }
  LogInfo << "End to Init FCOSDetectionPostProcess.";
  return APP_ERR_OK;
}

APP_ERROR FCOSPostProcess::DeInit() { return APP_ERR_OK; }

/*
    input:
        tensors:the output of mxpi_tensorinfer0 , the output of the model.
        objectInfos:save result.
    return:
        return the postprocess result.
*/
APP_ERROR FCOSPostProcess::Process(
    const std::vector<TensorBase> &tensors,
    std::vector<std::vector<ObjectInfo>> &objectInfos,
    const std::vector<ResizedImageInfo> &resizedImageInfos,
    const std::map<std::string, std::shared_ptr<void>> &configParamMap) {
  LogInfo << "Start to Process FCOSDetectionPostProcess.";
  APP_ERROR ret = APP_ERR_OK;
  auto inputs = tensors;
  ret = CheckAndMoveTensors(inputs);
  if (ret != APP_ERR_OK) {
    LogError << "CheckAndMoveTensors failed. ret=" << ret;
    return ret;
  }

  LogInfo << "FCOSDetectionPostProcess start to write results.";

  for (auto num : {0, 1}) {
    if ((num >= tensors.size()) || (num < 0)) {
      LogError << GetError(APP_ERR_INVALID_PARAM) << "TENSOR(" << num
               << ") must ben less than tensors'size(" << tensors.size()
               << ") and larger than 0.";
    }
  }
  LogInfo << "start to process.";
  auto shape = tensors[0].GetShape();
  cv::Mat res = cv::Mat(shape[1], shape[2], CV_32F, tensors[0].GetBuffer());
  cv::Mat classNum = cv::Mat(tensors[1].GetShape()[1], tensors[1].GetShape()[2],
                             CV_32S, tensors[1].GetBuffer());
  std::vector<ObjectInfo> objectInfo;
  int batchSize = shape[0];
  for (uint32_t i = 0; i < shape[1]; i++) {
    if (res.at<float>(i, CENTERPOINT) >= BESTMINCONF[classNum.at<int>(i, 0)]) {
      ObjectInfo objInfo;
      objInfo.x0 = res.at<float>(i, L);
      objInfo.y0 = res.at<float>(i, T);
      objInfo.x1 = res.at<float>(i, R);
      objInfo.y1 = res.at<float>(i, B);
      objInfo.confidence = res.at<float>(i, CENTERPOINT);
      objInfo.className = CATEGORIES[classNum.at<int>(i, 0)];
      objectInfo.push_back(objInfo);
    }
  }
  MxBase::NmsSort(objectInfo, RATE);
  objectInfos.push_back(objectInfo);
  LogInfo << "FCOSDetectionPostProcess write results successed.";
  LogInfo << "End to Process FCOSDetectionPostProcess.";
  return APP_ERR_OK;
}

extern "C" {
std::shared_ptr<MxBase::FCOSPostProcess> GetObjectInstance() {
  LogInfo << "Begin to get FCOSPostProcess instance.";
  auto instance = std::make_shared<MxBase::FCOSPostProcess>();
  LogInfo << "End to get FCOSPostProcess instance.";
  return instance;
}
}
}  // namespace MxBase